import 'package:flutter/material.dart';
import 'package:pretty_json/pretty_json.dart';
import 'package:device_information/device_information.dart';

class DeviceInformationView extends StatefulWidget {
  DeviceInformationView({Key? key}) : super(key: key);

  @override
  State<DeviceInformationView> createState() => _DeviceInformationViewState();
}

class _DeviceInformationViewState extends State<DeviceInformationView> {
  Map<String, dynamic> data = {};

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() async {
    try {
      data = {
        'platformVersion': await DeviceInformation.platformVersion,
        'imeiNo': await DeviceInformation.deviceIMEINumber,
        'modelName': await DeviceInformation.deviceModel,
        'manufacturer': await DeviceInformation.deviceManufacturer,
        'apiLevel': await DeviceInformation.apiLevel,
        'deviceName': await DeviceInformation.deviceName,
        'productName': await DeviceInformation.productName,
        'cpuType': await DeviceInformation.cpuName,
        'hardware': await DeviceInformation.hardware,
      };
      setState(() {});
    } catch (e) {
      debugPrint(e.toString());
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text('Data - DeviceInformationView:'),
        Text(
          prettyJson(
            data,
            indent: 2,
          ),
        ),
      ],
    );
  }
}
