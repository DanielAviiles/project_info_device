import 'dart:convert';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:pretty_json/pretty_json.dart';

class DeviceInfoPlusView extends StatefulWidget {
  DeviceInfoPlusView({Key? key}) : super(key: key);

  @override
  State<DeviceInfoPlusView> createState() => _DeviceInfoPlusViewState();
}

class _DeviceInfoPlusViewState extends State<DeviceInfoPlusView> {
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  late AndroidDeviceInfo? androidInfo;
  late BaseDeviceInfo? infoDevice;
  Map<String, dynamic> androidMapper = {};
  Map<String, dynamic> deviceInfoMapper = {};

  @override
  void didChangeDependencies() async {
    try {
      androidInfo = await deviceInfo.androidInfo;
      infoDevice = await deviceInfo.deviceInfo;
      androidMapper = androidInfo!.toMap();
      deviceInfoMapper = infoDevice!.toMap();

      final encodeMapAndroid = json.encode(androidMapper);
      final encodeMapDevice = json.encode(deviceInfoMapper);

      debugPrint(encodeMapAndroid);
      debugPrint(encodeMapDevice);
      setState(() {});
    } catch (e) {
      debugPrint(e.toString());
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('Data androidInfo - DeviceInfoPlugin:'),
        Text(prettyJson(androidMapper, indent: 2)),
        SizedBox(height: 10),
        Text('Data infoDevice - DeviceInfoPlugin:'),
        Text(prettyJson(deviceInfoMapper, indent: 2)),
      ],
    );
  }
}
